<?php

$result = 

$host = '';
$username = '';
$password = '';
$dbname = '';

$conn = new mysqli($host, $username, $password, $dbname);

if ($conn->connect_error) {
    http_response_code(500);
    die('Connect Error (' . $conn->connect_errno . ') '
      . $conn->connect_error);
}

switch ($_SERVER['REQUEST_METHOD']) {
    case "GET":
        $difficulty = $_GET["difficulty"];
        $count = $_GET["count"];

        if ($difficulty != 'easy' &&
            $difficulty != 'medium' &&
            $difficulty != 'hard') {
            http_response_code(400);
            echo "Invalid difficulty parameter in request!";
            exit;
        }

        if (!is_numeric($count)) {
            http_response_code(400);
            echo "Invalid count parameter in request!";
            exit;
        }

        $sql = "SELECT * " . 
               "FROM record_time " .
               "WHERE difficulty='" . $difficulty . "' " .
               "ORDER BY time ASC " .
               "LIMIT " . $count;

        $sql_return = $conn->query($sql);

        if (!$sql_return) {
            http_response_code(500);
            echo "Bad response from database!";
            exit;
        }

        $result = array();
        while($row = $sql_return->fetch_assoc())
            $result[] = $row;

        break;
    
    case "POST":
        $_POST = json_decode(file_get_contents('php://input'), true);
        $difficulty = $_POST['difficulty'];
        $player = $_POST['player'];
        $time = $_POST['time'];

        if ($difficulty != 'easy' &&
            $difficulty != 'medium' &&
            $difficulty != 'hard') {
            http_response_code(400);
            echo "Invalid difficulty parameter in request!";
            exit;
        }

        $player = $conn->real_escape_string($player);
        $player = htmlspecialchars($player);
        if (strlen($player) > 32) {
            http_response_code(400);
            echo "Invalid player parameter length in request!";
            exit;
        }

        if (!is_numeric($time)) {
            http_response_code(400);
            echo "Invalid time parameter in request!";
            exit;
        }

        $sql = "INSERT INTO record_time (difficulty, player, time) " .
               "VALUES ('" . $difficulty . "', '" . $player . "', '" . $time . "')";
    
        $sql_return = $conn->query($sql);
        if (!$sql_return) {
            http_response_code(500);
            echo "Bad response from database!";
            exit;
        }

        $result = $sql_return;

        break;

     case "PUT":
        break;

     case "DELETE":
        parse_str(file_get_contents("php://input"), $delete_vars);

        $difficulty = $delete_vars['difficulty'];
        $limit = $delete_vars['limit'];

        if ($difficulty != 'easy' &&
            $difficulty != 'medium' &&
            $difficulty != 'hard') {
            http_response_code(400);
            echo "Invalid difficulty parameter in request!";
            exit;
        }

        if (!is_numeric($limit)) {
            http_response_code(400);
            echo "Invalid limit parameter in request!";
            exit;
        }
    
        $sql = "DELETE FROM record_time WHERE " .
               "difficulty='" . $difficulty . "' AND time > " .
               "(" .
               "SELECT MAX(sub.time) FROM " .
               "(" .
               "SELECT * FROM record_time " .
                   "WHERE difficulty='" . $difficulty . "' " .
                   "ORDER BY time ASC LIMIT " . $limit .
               ") sub " .
               ")";

        $sql_return = $conn->query($sql);
        if (!$sql_return) {
            http_response_code(500);
            echo "Bad response from database!";
            exit;
        }

        $result = $sql_return;

        break;
}

$conn->close();

$json = json_encode($result);
echo $json;

?>
