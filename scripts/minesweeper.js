﻿/**
 * @license
 * Copyright (c) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/**
 * @file Implements classic minesweeper game from Windows OS.
 * @copyright 2016 Paavo Markkola <paavo.markkola@iki.fi>
 * @author Paavo Markkola <paavo.markkola@iki.fi>
 */
/**
 * Game state when the game is over.
 * @constant
 */
const GAME_OVER = 0;
/**
 * Game state when the game can start.
 * @constant
 */
const GAME_READY = 1;
/**
 * Game state when the game is ongoing.
 * @constant
 */
const GAME_STARTED = 2;
/**
 * Game state when the player has won.
 * @constant
 */
const GAME_WIN = 3;

/**
 * Constant for easy game difficulty.
 * @constant
 * @type {String}
 */
const GAME_EASY = "easy";
/**
 * Constant for medium game difficulty.
 * @constant
 * @type {String}
 */
const GAME_MEDIUM = "medium";
/**
 * Constant for hard game difficulty.
 * @constant
 * @type {String}
 */
const GAME_HARD = "hard";

/**
 * Number of rows on easy difficulty.
 * @constant
 * @default
 */
const ROWS_EASY = 9;
/**
 * Number of columns on easy difficulty.
 * @constant
 * @default
 */
const COLUMNS_EASY = 9;
/**
 * Number of mines on easy difficulty.
 * @constant
 * @default
 */
const MINES_EASY = 10;

/**
 * Number of rows on medium difficulty.
 * @constant
 * @default
 */
const ROWS_MEDIUM = 16;
/**
 * Number of columns on medium difficulty.
 * @constant
 * @default
 */
const COLUMNS_MEDIUM = 16;
/**
 * Number of mines on medium difficulty.
 * @constant
 * @default
 */
const MINES_MEDIUM = 40;

/**
 * Number of rows on hard difficulty.
 * @constant
 * @default
 */
const ROWS_HARD = 16;
/**
 * Number of columns on hard difficulty.
 * @constant
 * @default
 */
const COLUMNS_HARD = 30;
/**
 * Number of mines on hard difficulty.
 * @constant
 * @default
 */
const MINES_HARD = 99;

/**
 * Constant for tile type when the tile is a mine.
 * @constant
 */
const TILE_TYPE_MINE = 10;
/**
 * Constant for tile type when the tile is a unknown.
 * @constant
 */
const TILE_TYPE_UNKNOWN = 11;
/**
 * Constant for tile type when the tile is flagged.
 * @constant
 */
const TILE_TYPE_FLAGGED = 12;
/**
 * Constant for tile type when the tile is incorrectly flagged.
 * @constant
 */
const TILE_TYPE_WRONG_FLAG = 13;
/**
 * Constant for tile type when the tile is empty.
 * @constant
 */
const TILE_TYPE_EMPTY = 0;
/**
 * Constant for tile type when there is one mine near by.
 * @constant
 */
const TILE_TYPE_ONE = 1;
/**
 * Constant for tile type when there are two mines near by.
 * @constant
 */
const TILE_TYPE_TWO = 2;
/**
 * Constant for tile type when there are three mines near by.
 * @constant
 */
const TILE_TYPE_THREE = 3;
/**
 * Constant for tile type when there are four mines near by.
 * @constant
 */
const TILE_TYPE_FOUR = 4;
/**
 * Constant for tile type when there are five mines near by.
 * @constant
 */
const TILE_TYPE_FIVE = 5;
/**
 * Constant for tile type when there are six mines near by.
 * @constant
 */
const TILE_TYPE_SIX = 6;
/**
 * Constant for tile type when there are seven mines near by.
 * @constant
 */
const TILE_TYPE_SEVEN = 7;
/**
 * Constant for tile type when there are eight mines near by.
 * @constant
 */
const TILE_TYPE_EIGHT = 8;
/**
 * Constant for number of items in the top list when listing record times.
 * @constant
 */
const TOP_LIST_COUNT = 10;

/**
 * MinesweeperGame class represents a game of minesweeper. 
 * @constructor
 * @author Paavo Markkola <paavo.markkola@iki.fi>
 */
function MinesweeperGame() {
    if (!(this instanceof MinesweeperGame))
        return new MinesweeperGame();
    /** 
     * Difficulty setting of the game.
     * @member {String} 
     */
    this.difficulty;
    /** 
     * {@link GameBoard} instance that contains tiles making up the minesweeper game.
     * @member {GameBoard} 
     */
    this.board;
    /** 
     * Number representing the state of the game.
     * @member {Number} 
     */
    this.state;

    this.setNewGame();
}

/**
 * Create new {@link GameBoard} for a minesweeper game. The dimensions of the
 * game board depend on the difficulty setting.
 * @param {String} difficulty - Difficulty setting of the minesweeper game.
 */
MinesweeperGame.prototype.createBoard = function (difficulty) {
    if (difficulty == GAME_MEDIUM) {
        this.board = new GameBoard(ROWS_MEDIUM, COLUMNS_MEDIUM, MINES_MEDIUM);
        this.difficulty = GAME_MEDIUM;
    } else if (difficulty == GAME_HARD) {
        this.board = new GameBoard(ROWS_HARD, COLUMNS_HARD, MINES_HARD);
        this.difficulty = GAME_HARD;
    } else {
        this.board = new GameBoard(ROWS_EASY, COLUMNS_EASY, MINES_EASY);
        this.difficulty = GAME_EASY;
    }
};

/**
 * Change the difficulty setting of the minesweeper game. Only recreate the
 * {@link GameBoard} if the difficulty has actually changed. 
 * @param {String} difficulty - New difficulty setting for the minesweeper game.
 */
MinesweeperGame.prototype.changeGameDifficulty = function (difficulty) {
    if (this.difficulty != difficulty && (difficulty == GAME_EASY ||
                                          difficulty == GAME_MEDIUM ||
                                          difficulty == GAME_HARD))
        this.createBoard(difficulty);
};

/**
 * Returns current state of the minesweeper game.
 * @returns {Number} The current state of the minesweeper game.
 */
MinesweeperGame.prototype.getState = function () {
    return this.state;
};

/**
 * Set up a new minesweeper game of given difficulty. The game board is created
 * and initialized but not set as players should not be able to make the first
 * click on a mine.
 * @param {String} difficulty - Difficulty setting for the minesweeper game.
 */
MinesweeperGame.prototype.setNewGame = function (difficulty) {
    if (this.difficulty == undefined)
        this.createBoard(difficulty);
    else
        this.changeGameDifficulty(difficulty);
    this.board.clearBoard();
    this.board.initMinesLeft();
    this.state = GAME_READY;
};

/**
 * Start a minesweeper game. The tile that the player clicked is given as
 * parameter. 
 * @param {BoardTile} tile - Tile that the player clicked to start the game. 
 */
MinesweeperGame.prototype.setStartGame = function (tile) {
    this.board.setBoard(tile);
    this.state = GAME_STARTED;
};

/**
 * Set game over state. Reveal all mines and incorrectly placed flags to
 * the player.
 */
MinesweeperGame.prototype.setGameOver = function () {
    this.board.revealMines();
    this.board.checkFlagged();
    this.state = GAME_OVER;
};

/**
 * Set game won status. Reveal all mines to the player.
 */
MinesweeperGame.prototype.setGameWin = function () {
    this.board.revealMines();
    this.state = GAME_WIN;
};

/**
 * Handles a click on an empty type {@link BoardTile} given as parameter.
 * Reveal all tiles next to the empty tile and check whole game board if
 * new empty tiles have been revealed in the process.
 * @param {BoardTile} tile - The empty tile that the player clicked.
 */
MinesweeperGame.prototype.handleClickOnEmptyTile = function (tile) {
    this.board.turnEmptyTile(tile);
    this.board.checkTurnedEmptyTiles();
};

/**
 * Called when the player clicks on a tile given as parameter. Performs
 * actions required by the click and returns current state of the game to 
 * the caller.
 * @param {BoardTile} tile - The tile that the player has clicked.
 * @returns {Number} State of the current game.
 */
MinesweeperGame.prototype.clickTile = function (tile) {
    if (this.getState() == GAME_OVER || this.getState() == GAME_WIN)
        return this.getState();

    if (this.getState() == GAME_READY)
        this.setStartGame(tile);

    if (tile.isFlagged())
        return this.getState();

    this.board.turnTile(tile);

    if (tile.getType() == TILE_TYPE_EMPTY)
        this.handleClickOnEmptyTile(tile);

    if (tile.getType() == TILE_TYPE_MINE)
        this.setGameOver();
    else if (this.board.checkCleared())
        this.setGameWin();

    return this.getState();
};

/**
 * Called when the player right clicks, i.e., flags, a tile which is given
 * as parameter. A flag on a tile prevents the player from clicking the tile.
 * Flagging a tile that is already flagged removes the flag and a tile that
 * has already been clicked cannot be flagged.
 * @param {BoardTile} tile - Tile that the player has flagged.
 */
MinesweeperGame.prototype.flagTile = function (tile) {
    if (this.getState() == GAME_OVER ||
        this.getState() == GAME_WIN ||
        tile.isClicked())
        return;
    if (tile.isFlagged()) {
        tile.setFlagged(false);
        this.board.increaseMinesLeft();
    } else if (this.board.getMinesLeft() > 0) {
        tile.setFlagged(true);
        this.board.decreaseMinesLeft();
    }
};

/**
 * Called when the player double clicks on a tile given as parameter. Performs
 * actions required by the double click and returns current state of the game
 * to the caller.
 * @param {BoardTile} tile - The tile that the player has clicked.
 * @returns {Number} State of the current game.
 */
MinesweeperGame.prototype.doubleClickTile = function (tile) {
    // Double click has no function if the is not ongoing.
    if (this.getState() == GAME_OVER ||
        this.getState() == GAME_WIN ||
        this.getState() == GAME_READY)
        return this.getState();

    // Double click has no effect on unturned, flagged or empty tiles.
    if (tile.isFlagged() ||
        !tile.isClicked() ||
        tile.getType() == TILE_TYPE_EMPTY)
        return this.getState();

    // Double click has no effect if incorrect number of mines are flagged
    // in the neighborhood.
    if (tile.getType() != this.board.countFlaggedTilesInNeighborhood(tile))
        return this.getState();

    // Go through the neighborhood and turn all tiles that are not flagged.
    for (var r = tile.row - 1; r <= tile.row + 1; r++) {
        for (var c = tile.column - 1; c <= tile.column + 1; c++) {
            var neighbor = this.board.getTile(r, c);

            if (neighbor == null)
                continue;

            if (neighbor.isFlagged())
                continue;

            this.board.turnTile(neighbor);

            if (neighbor.getType() == TILE_TYPE_EMPTY)
                this.handleClickOnEmptyTile(tile);

            if (neighbor.getType() == TILE_TYPE_MINE) {
                this.setGameOver();
                return this.getState();
            }
        }
    }

    if (this.board.checkCleared()) {
        this.setGameWin();
    }

    return this.getState();
};

/**
 * Return the number of mines on board that the user has not flagged.
 * @returns {Number} The number of unflagged mines on board.
 */
MinesweeperGame.prototype.getMinesLeft = function () {
    return this.board.getMinesLeft();
};


/**
 * GameBoard class represents a board that arranges all tiles that a player can
 * click in a game of minesweeper. Tiles are organized in a two dimensional
 * array. The size of the array depends on the given parameters. 
 * @constructor
 * @param {Number} rows - The number of tile rows on the game board.
 * @param {Number} columns - The number of tile columns on the game board. 
 * @param {Number} mines - The number of mines on the game board. 
 * @author Paavo Markkola <paavo.markkola@iki.fi>
 */
function GameBoard(rows, columns, mines) {
    /** 
     * Number of horizontal rows on the game board.
     * @member {Number} 
     */
    this.rows = rows;
    /** 
     * Number of vertical columns on the game board.
     * @member {Number} 
     */
    this.columns = columns;
    /** 
     * Number of mines on the game board.
     * @member {Number} 
     */
    this.mines = mines;
    /**
     * Number of mines that have not been flagged.
     * @member {Number}
     */
    this.minesLeft = mines;
    /** 
     * Two dimensional array containing all tiles on the game board.
     * @member {Array} 
     */
    this.table = [];
    this.initBoard();
}

/**
 * Initializes the game board by creating a two dimensional array of 
 * {@link BoardTile} objects.
 */
GameBoard.prototype.initBoard = function () {
    for (var row = 0; row < this.rows; row++) {
        var tileRow = [];
        for (var column = 0; column < this.columns; column++) {
            var tile = new BoardTile(row, column);
            tileRow.push(tile);
        }
        this.table.push(tileRow);
    }
}

/**
 * Returns {@link BoardTile} object corresponding to the row and column tile
 * specified in parameters. Returns null if the tile does not exist.
 * @param {Number} row - The row of the tile. 
 * @param {Number} column - The column of the tile. 
 * @returns {BoardTile} The tile object, or null if the tile does not exist.
 */
GameBoard.prototype.getTile = function (row, column) {
    if ((row >= 0 && row < this.rows) &&
        (column >= 0 && column < this.columns)) {
        return this.table[row][column];
    } else
        return null;
};

/**
 * Clears the game board by setting default values to all {@link BoardTile}
 * objects. This method is called after the game board is created and when a
 * new game is being initialized. 
 */
GameBoard.prototype.clearBoard = function () {
    for (var row = 0; row < this.rows; row++) {
        for (var column = 0; column < this.columns; column++) {
            var tile = this.getTile(row, column);
            tile.setType(TILE_TYPE_EMPTY);
            tile.setStatus(TILE_TYPE_EMPTY);
            tile.setClicked(false);
            tile.setEmptyHandled(false);
            tile.setFlagged(false);
        }
    }
};

/**
 * Places mines randomly on the game board. This is called when the player
 * first clicks any tile on the game board after starting a new game. Mines
 * are not set before in order to prevent the player from hitting a mine in
 * the first tile. 
 * @param {BoardTile} excludeTile - The tile that the player made
 * the first click on. 
 */
GameBoard.prototype.setBoard = function (excludeTile) {
    var freeTiles = this.rows * this.columns - 1;
    var randomTile;
    var tileArray = [];
    var tile;

    // Create an array which contains an index of each possible tile.
    for (var i = 0; i < this.rows * this.columns; i++) {
        tileArray.push(i);
    }

    // Remove the tile the player clicked on from the array.
    tileArray.splice(excludeTile.row * this.columns + excludeTile.column, 1);

    this.clearBoard();

    for (var mine = 0; mine < this.mines; mine++) {
        randomTile = Math.round(Math.random() * freeTiles);
        tile = this.getTile(Math.floor(tileArray[randomTile] / this.columns),
                            tileArray[randomTile] % this.columns);
        tile.setType(TILE_TYPE_MINE);
        tileArray.splice(randomTile, 1);
        freeTiles--;
    }

    this.setNearMineCounts();
};

/**
 * Sets correct type for tiles that are not mines. Does this by counting the
 * number of mines next to the tile in any direction.
 */
GameBoard.prototype.setNearMineCounts = function () {
    for (row = 0; row < this.rows; row++) {
        for (column = 0; column < this.columns; column++) {
            var tile = this.getTile(row, column);
            if (tile.getType() != TILE_TYPE_MINE)
                tile.setType(this.countMinesInNeighborhood(tile));
        }
    }
};

/**
 * Counts mines that are next to the specified tile and returns the result.
 * @param {BoardTile} tile - The tile object being inspected for mines. 
 * @returns {Number} The number of mines next to the tile.
 */
GameBoard.prototype.countMinesInNeighborhood = function (tile) {
    var count = 0;
    var neighbor;
    for (var r = tile.row - 1; r <= tile.row + 1; r++) {
        for (var c = tile.column - 1; c <= tile.column + 1; c++) {
            neighbor = this.getTile(r, c);
            if (neighbor != null && neighbor.getType() == TILE_TYPE_MINE)
                count++;
        }
    }
    return count;
};

/**
 * Counts flagged tiles that are next to the specified tile and returns the
 * result.
 * @param {BoardTile} tile - The tile object being inspected for flags. 
 * @returns {Number} The number of flagged tiles next to the tile.
 */
GameBoard.prototype.countFlaggedTilesInNeighborhood = function (tile) {
    var count = 0;
    var neighbor;
    for (var r = tile.row - 1; r <= tile.row + 1; r++) {
        for (var c = tile.column - 1; c <= tile.column + 1; c++) {
            neighbor = this.getTile(r, c);
            if (neighbor != null && neighbor.isFlagged())
                count++;
        }
    }

    return count;
};

/**
 * Handles a click on an empty tile. An empty tile has no mines next to it so
 * it is safe to click all tiles next ot it. This method automatically reveals
 * tiles next o the empty tile for the player. Marks this tile as handled so
 * that it is not checked again.
 * @param {BoardTile} tile - The tile object being handled. 
 */
GameBoard.prototype.turnEmptyTile = function (tile) {
    var neighbor;
    for (var r = tile.row - 1; r <= tile.row + 1; r++) {
        for (var c = tile.column - 1; c <= tile.column + 1; c++) {
            neighbor = this.getTile(r, c);
            if (neighbor != null && neighbor.getType() != TILE_TYPE_MINE)
                this.turnTile(neighbor);
        }
    }
    tile.setEmptyHandled(true);
};

/**
 * Loop through the game board and handle all revealed empty tiles. That is,
 * revealing an empty tile might have revealed more empty tiles since all it's
 * neighbors are automatically revealed. Continue loop until all exposed empty
 * tiles have been handled.
 */
GameBoard.prototype.checkTurnedEmptyTiles = function () {
    var stop = false;
    while (!stop) {
        stop = true;
        for (var row = 0; row < this.rows; row++) {
            for (var column = 0; column < this.columns; column++) {
                var tile = this.getTile(row, column);
                if (tile.isClicked() &&
                    tile.getType() == TILE_TYPE_EMPTY &&
                    !tile.isEmptyHandled()) {
                    this.turnEmptyTile(tile);
                    stop = false;
                }
            }
        }
    }
};

/**
 * Turns a specific tile by changing its status and by marking that tile
 * as clicked.
 * @param {BoardTile} tile - The tile object being turned. 
 */
GameBoard.prototype.turnTile = function (tile) {
    tile.setClicked(true);
    tile.setStatus(tile.getType());
};

/**
 * Checks if all tiles that are not mines have been turned by the player.
 * Returns true if player has turned all tiles, i.e., the player has won,
 * and false if the game should continue. 
 * @returns {Boolean} True if all tiles turned, false otherwise.
 */
GameBoard.prototype.checkCleared = function () {
    for (var row = 0; row < this.rows; row++) {
        for (var column = 0; column < this.columns; column++) {
            var tile = this.getTile(row, column);
            if (tile.getType() != TILE_TYPE_MINE &&
                !tile.isClicked())
                return false;
        }
    }
    return true;
};

/**
 * Checks if there are tiles that have been flagged but are not mines. For
 * each wrongly flagged tile changes image source to indicate this. Called
 * when player has hit a mine and game is over to show the tiles that the
 * player has incorrectly flagged.
 */
GameBoard.prototype.checkFlagged = function () {
    for (var row = 0; row < this.rows; row++) {
        for (var column = 0; column < this.columns; column++) {
            var tile = this.getTile(row, column);
            if (tile.getType() != TILE_TYPE_MINE &&
                tile.isFlagged())
                tile.setStatus(TILE_TYPE_WRONG_FLAG);
        }
    }
};

/**
 * Reveals all tiles that are mines and not flagged. Used when game is over
 * and location of all mines is revealed to the player.
 */
GameBoard.prototype.revealMines = function () {
    for (var row = 0; row < this.rows; row++) {
        for (var column = 0; column < this.columns; column++) {
            var tile = this.getTile(row, column);
            if (tile.getType() == TILE_TYPE_MINE &&
                !tile.isFlagged())
                this.turnTile(tile);
        }
    }
};

/**
 * Sets initialial value for how many mines are left unflagged. This is always
 * the same as number of mines initially on board.
 */
GameBoard.prototype.initMinesLeft = function () {
    this.minesLeft = this.mines;
};

/**
 * Decreases the number of mines unflagged on board by one. Natuarally there
 * can be no less than zero unflagged mines. Used when player flags a tile.
 */
GameBoard.prototype.decreaseMinesLeft = function () {
    if (this.minesLeft > 0)
        this.minesLeft--;
};

/**
 * Increases the number of mines unflagged on board by one. Does not increase
 * the number beyond the actual number of mines on board. Used when player
 * unflags a tile.
 */
GameBoard.prototype.increaseMinesLeft = function () {
    if (this.minesLeft < this.mines)
        this.minesLeft++;
};

/**
 * Return the number of mines on board that the user has not flagged.
 * @returns {Number} The number of unflagged mines on board.
 */
GameBoard.prototype.getMinesLeft = function () {
    return this.minesLeft;
};


/**
 * BoardTile class represents a single clickable tile in a minesweeper game.
 * @constructor
 * @author Paavo Markkola <paavo.markkola@iki.fi>
 */
function BoardTile(row, column) {
    /** 
     * Horizontal row of the tile.
     * @member {Number} 
     */
    this.row = row;
    /** 
     * Vertical column of the tile.
     * @member {Number} 
     */
    this.column = column;
    /** 
     * Type of the tile, i.e., mine or number indicating number of mines next
     * to it.
     * @member {Number} 
     */
    this.type = TILE_TYPE_EMPTY;
    /**
     * Status of the tile. Same as tile type except that status reflects the
     * state of the tile, i.e., what user sees.
     * @member {Number} 
     */
    this.status = TILE_TYPE_UNKNOWN;
    /** 
     * Boolean member indicating whether the player has clicked the tile.
     * @member {Boolean} 
     */
    this.clicked = false;
    /** 
     * Boolean member indicating whether the empty tile has been handled.
     * Valid only if the type of the tile is empty.
     * @member {Boolean} 
     */
    this.emptyHandled = false;
    /** 
     * Boolean member indicating whether the player has flagged the tile.
     * @member {Boolean} 
     */
    this.flagged = false;
}

/**
 * Returns tile type.
 * @returns {Number} The type of the tile.
 */
BoardTile.prototype.getType = function () {
    return this.type;
};

/**
 * Sets tile type.
 * @param {Number} type - Desired type of the tile. 
 */
BoardTile.prototype.setType = function (type) {
    this.type = type;
};

/**
 * Returns tile status.
 * @returns {Number} The status of the tile.
 */
BoardTile.prototype.getStatus = function () {
    return this.status;
};

/**
 * Sets tile status.
 * @param {Number} status - Desired status of the tile. 
 */
BoardTile.prototype.setStatus = function (status) {
    this.status = status;
};

/**
 * Checks if tile has been clicked or not.
 * @returns {Boolean} True if the tile has been clicked and false otherwise.
 */
BoardTile.prototype.isClicked = function () {
    return this.clicked;
};

/**
 * Sets clicked status of the tile.
 * @param {Boolean} value - Clicked status of the tile. 
 */
BoardTile.prototype.setClicked = function (value) {
    this.clicked = value;
};

/**
 * Checks if tile has been handled for being empty or not.
 * @returns {Boolean} True if the tile has been handled and false otherwise.
 */
BoardTile.prototype.isEmptyHandled = function () {
    return this.emptyHandled;
};

/**
 * Sets empty handled status of the tile.
 * @param {Boolean} value - Empty handled status of the tile. 
 */
BoardTile.prototype.setEmptyHandled = function (value) {
    this.emptyHandled = value;
};

/**
 * Checks if tile has been flagged or not.
 * @returns {Boolean} True if the tile has been flagged and false otherwise.
 */
BoardTile.prototype.isFlagged = function () {
    return this.flagged;
};

/**
 * Sets flagged status of the tile.
 * @param {Boolean} value - Flagged status of the tile. 
 */
BoardTile.prototype.setFlagged = function (value) {
    this.flagged = value;
    if (value)
        this.setStatus(TILE_TYPE_FLAGGED);
    else
        this.setStatus(TILE_TYPE_UNKNOWN);
};

/**
 * Minesweeper module
 */
var minesweeperApp = angular.module('minesweeperApp', ['ngRoute']);

/**
 * Configure routing for the Minesweeper app.
 */
minesweeperApp.config(function ($routeProvider) {
    $routeProvider
        // Root content is the main game page.
        .when('/', {
            templateUrl: 'pages/main.html',
            controller: 'minesweeperController'
        })
        // Starts new game by sending message to minesweeperController and
        // redirecting back to root content.
        .when('/new_game', {
            resolve: {
                function ($rootScope, $location) {
                    $rootScope.$broadcast('StartNewGame');
                    $location.path('/');
                }
            }
        })
        // Route to options page.
        .when('/options', {
            templateUrl: 'pages/options.html',
            controller: 'optionsController'
        })
        // Zooms in using boardStyle service to increase board width.
        // Redirects back to root content.
        .when('/zoom_in', {
            resolve: {
                function ($location, boardStyle) {
                    boardStyle.zoomIn();
                    $location.path('/');
                }
            }
        })
        // Zooms out using boardStyle service to decrease board width.
        // Redirects back to root content.
        .when('/zoom_out', {
            resolve: {
                function ($location, boardStyle) {
                    boardStyle.zoomOut();
                    $location.path('/');
                }
            }
        })
        // Route to help page.
        .when('/help', {
            templateUrl: 'pages/help.html',
            controller: 'helpController'
        })
        // Route to best times page.
        .when('/records', {
            templateUrl: 'pages/records.html',
            controller: 'recordsController'
        })
        .otherwise('/');
});

/**
 * gameData custom service is used to store current game data. This is needed
 * to keep the game data intact in case player exits root content page and
 * returns back.
 */
minesweeperApp.factory('gameData',
    function () {
        var factory = {};

        factory.game = null;
        factory.difficulty = GAME_EASY;

        factory.createGameData = function () {
            factory.game = new MinesweeperGame();
            return factory.game;
        };

        factory.getDifficulty = function () {
            return this.difficulty;
        };

        factory.setDifficulty = function (difficulty) {
            this.difficulty = difficulty;
        };

        factory.setGameData = function (game) {
            this.game = game;
        };

        factory.getGameData = function () {
            return this.game;
        };

        return factory;
    });

/**
 * gameTime custom service is used to implement the game clock. The service
 * includes methods to manipulate game clock and to retrieve current game
 * time as tenths of a second.
 */
minesweeperApp.factory('gameTime', ['$interval',
    function ($interval) {
        var factory = {};
        var time = 0;
        var timer = null;

        var timerFunc = function () {
            time++;
        };

        factory.getTime = function () {
            return time;
        };

        factory.resetTime = function () {
            time = 0;
        };

        factory.startTimer = function () {
            if (timer != null)
                return;
            factory.stopTimer();
            timer = $interval(timerFunc, 100);
        };

        factory.stopTimer = function () {
            $interval.cancel(timer);
            timer = null;
        };

        return factory;
    }]);

/**
 * boardStyle service is used to build the style for the minesweeper game
 * board. Board size varies with zoom level and the game difficulty.
 */
minesweeperApp.factory('boardStyle',
    function () {
        var factory = {};

        factory.zoomLevel = 3;
        factory.rows = ROWS_EASY;
        factory.columns = COLUMNS_EASY;

        factory.updateBoardSize = function (rows, columns) {
            this.rows = rows;
            this.columns = columns;
        };

        factory.zoomIn = function () {
            if (this.zoomLevel < 6)
                this.zoomLevel++;
        };

        factory.zoomOut = function () {
            if (this.zoomLevel > 1)
                this.zoomLevel--;
        };

        factory.getTileSize = function () {
            return this.zoomLevel * 10 + 'px';
        };

        factory.getBoardStyle = function () {
            return {
                "height": this.zoomLevel * 10 * this.rows + "px",
                "width": this.zoomLevel * 10 * this.columns + "px",
                "margin": "0 auto"
            }
        };

        return factory;
    });

/**
 * recordTimes service contains methods to store and retrieve best times from
 * the database. Uses $http service to connec to the backend.
 */
minesweeperApp.factory('recordTimes', ['$http', '$httpParamSerializerJQLike',
    function ($http, $httpParamSerializerJQLike) {
        var factory = {};

        factory.apiUrl = 'api/api.php';

        /**
         * Get best times from database with GET request. Get limit times
         * to selected difficulty and given number of times.
         */
        factory.getBestTimes = function (difficulty, count) {
            var url = this.apiUrl
                      + '?difficulty=' + difficulty
                      + '&count=' + count;
            return $http.get(url).then(
                function (response) {
                    return response.data;
                },
                function (response) {
                    return null;
                });
        };

        /**
         * Set new best time entry to the database with http POST request.
         * Date for the record is automatically added byt the database.
         */
        factory.setBestTime = function (difficulty, player, time) {
            var data = {
                difficulty: difficulty,
                player: player,
                time: time
            };
            return $http.post(this.apiUrl,
                              JSON.stringify(data),
                              {
                                  headers: {
                                      'Content-Type': 'application/json'
                                  }
                              }).then(
                function (response) {
                    return true;
                },
                function (response) {
                    return false;
                });
        };
        /**
         * Check if given time is within the top list of records. This depends
         * on difficulty setting and how many records are included in the top
         * list.
         */
        factory.checkIfTimeInTop = function (difficulty, time, count) {
            return this.getBestTimes(difficulty, count).then(
                function (data) {
                    for (var index = 0; index < data.length; index++) {
                        if (time < data[index].time)
                            return true;
                    }
                    if (index < count)
                        return true;
                    return false;
                },
                function (data) {
                    return false;
                });
        };

        /**
         * Remove records from database that do not fit with selected top
         * list of times for a difficulty.
         */
        factory.deletedOldRecords = function (difficulty, limit) {
            var data = {
                difficulty: difficulty,
                limit: limit
            };
            return $http(
                {
                    method: 'DELETE',
                    url: this.apiUrl,
                    data: $httpParamSerializerJQLike(data),
                    headers: {
                        'Content-type': 'application/x-www-form-urlencoded'
                    }
                }).then(
                function (response) {
                    return true;
                }, function (rejection) {
                    return false;
                });
        };

        return factory;
    }]);


/**
 * gameTimeString filter formats game time in to a more readable string
 * format of mins:secs:tenths.
 */
minesweeperApp.filter('gameTimeString',
    function () {
        return function (time) {
            var min = Math.floor(time / 10 / 60);
            var sec = Math.floor(time / 10 % 60);
            if (min < 10) min = '0' + min;
            if (sec < 10) sec = '0' + sec;
            return min + ':' + sec + ':' + time % 10;
        };

    });

/**
 * gameStateImg filter translates game state to corresponding smiley image.
 */
minesweeperApp.filter('gameStateImg',
    function () {
        return function (state) {
            if (state == GAME_OVER)
                return 'images/smileySad.png';
            else if (state == GAME_WIN)
                return 'images/smileyShades.png';
            else
                return 'images/smiley.png';
        };
    });

/**
 * tileStatus filter translates tile status to corresponding tile image.
 */
minesweeperApp.filter('tileStatus',
    function () {
        return function (state) {
            if (state == TILE_TYPE_MINE)
                return 'images/mineTile.png';
            else if (state == TILE_TYPE_UNKNOWN)
                return 'images/unknownTile.png';
            else if (state == TILE_TYPE_FLAGGED)
                return 'images/flagTile.png';
            else if (state == TILE_TYPE_FLAGGED)
                return 'images/flagTile.png';
            else if (state == TILE_TYPE_WRONG_FLAG)
                return 'images/wrongFlagTile.png'
            else if (state == TILE_TYPE_EMPTY)
                return 'images/emptyTile.png';
            else if (state == TILE_TYPE_ONE)
                return 'images/oneTile.png';
            else if (state == TILE_TYPE_TWO)
                return 'images/twoTile.png';
            else if (state == TILE_TYPE_THREE)
                return 'images/threeTile.png';
            else if (state == TILE_TYPE_FOUR)
                return 'images/fourTile.png';
            else if (state == TILE_TYPE_FIVE)
                return 'images/fiveTile.png';
            else if (state == TILE_TYPE_SIX)
                return 'images/sixTile.png';
            else if (state == TILE_TYPE_SEVEN)
                return 'images/sevenTile.png';
            else if (state == TILE_TYPE_EIGHT)
                return 'images/eightTile.png';
        };
    });

/**
 * gameSize filter returns a string describing the game difficulty either in
 * terms of number of mines or total number of tiles.
 */
minesweeperApp.filter('gameSize',
    function () {
        return function (difficulty, param) {
            if (param == 'mines') {
                if (difficulty == GAME_MEDIUM) return MINES_MEDIUM + ' mines';
                else if (difficulty == GAME_HARD) return MINES_HARD + ' mines';
                else return MINES_EASY + ' mines';
            } else if (param == 'tiles') {
                if (difficulty == GAME_MEDIUM) return ROWS_MEDIUM + ' x ' + COLUMNS_MEDIUM + ' tiles';
                else if (difficulty == GAME_HARD) return ROWS_HARD + ' x ' + COLUMNS_HARD + ' tiles';
                else return ROWS_EASY + ' x ' + COLUMNS_EASY + ' tiles';
            }
        }
    });

/**
 * Filter to translate date received from database. This is
 * needed for AngularJS actual date filter to work.
 */
minesweeperApp.filter('dateConvert', function () {
    return function (input) {
        var t = String(input).split(/[- :]/);
        return new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
    };
});

minesweeperApp.controller('menuController', function ($scope, $rootScope) {
    $scope.menuShow = false;
    $scope.menuLocation = 'Minesweeper';
    $rootScope.$on('menuEvent', function (event, data) {
        $scope.menuLocation = 'Minesweeper'
        if (data != '') {
            $scope.menuLocation += ' > '
            $scope.menuLocation += data;
        }
    });
});

minesweeperApp.controller('helpController', function ($scope, $rootScope) {
    $rootScope.$broadcast('menuEvent', 'help');
});

minesweeperApp.controller('optionsController', function ($scope, $rootScope, $location, gameData) {
    $rootScope.$broadcast('menuEvent', 'options');

    $scope.difficulty = gameData.getDifficulty();

    $scope.applyOptions = function () {
        if ($scope.difficulty != gameData.getDifficulty()) {
            gameData.setDifficulty($scope.difficulty);
            $rootScope.$broadcast('StartNewGame');
        }
        $location.path('/');
    }

    $scope.cancelOptions = function () {
        $location.path('/');
    }
});

minesweeperApp.controller('recordsController', function ($scope, $rootScope, gameData, recordTimes) {
    $rootScope.$broadcast('menuEvent', 'best times');

    $scope.$watch('difficulty', function () {
        recordTimes.getBestTimes($scope.difficulty, TOP_LIST_COUNT).then(
            function (data) {
                $scope.records = data;
        });
    });

    $scope.difficulty = gameData.getDifficulty();
});

minesweeperApp.controller("minesweeperController",
    function ($scope, $rootScope, gameData, gameTime, boardStyle, recordTimes) {
        $scope.getGameState = function () {
            return $scope.game.getState();
        };

        $scope.getGameTime = function () {
            return gameTime.getTime();
        };

        $scope.getBoardStyle = function () {
            return boardStyle.getBoardStyle();
        };

        $scope.startNewGame = function () {
            $scope.game.setNewGame(gameData.getDifficulty());
            boardStyle.updateBoardSize($scope.game.board.rows,
                           $scope.game.board.columns);
            gameTime.stopTimer();
            gameTime.resetTime();
        };

        $scope.getMinesLeft = function () {
            return $scope.game.getMinesLeft();
        };

        $scope.getTileStatus = function (tile) {
            return tile.getStatus();
        };

        $scope.flagTile = function (tile) {
            $scope.game.flagTile(tile);
        };

        $scope.clickTile = function (tile) {
            var state = $scope.game.clickTile(tile);
            if (state == GAME_OVER)
                gameTime.stopTimer();
            else if (state == GAME_WIN) {
                gameTime.stopTimer();
                recordTimes.checkIfTimeInTop($scope.game.difficulty,
                                             gameTime.getTime(),
                                             TOP_LIST_COUNT).then(
                    function (included) {
                        if (included) {
                            var player = prompt("You made it to top ten! Plaese enter your name:", "Player");
                            if (player == null) {
                                player = 'Player';
                            }
                            recordTimes.setBestTime($scope.game.difficulty,
                                                    player,
                                                    gameTime.getTime()).then(
                                function (status) {
                                    if (status)
                                        recordTimes.deletedOldRecords($scope.game.difficulty,
                                                                      TOP_LIST_COUNT);
                                });
                        }
                    });
            } else if (state == GAME_STARTED)
                gameTime.startTimer();
        }

        $scope.doubleClickTile = function (tile) {
            var state = $scope.game.doubleClickTile(tile);
            if (state == GAME_OVER || state == GAME_WIN)
                gameTime.stopTimer();
        };

        $scope.getTileSize = function () {
            return boardStyle.getTileSize();
        };

        $rootScope.$on('StartNewGame', function (event, data) {
            $scope.startNewGame();
        });

        $rootScope.$on("$locationChangeStart", function () {
            gameTime.stopTimer();
            gameData.setGameData($scope.game, $scope.gameDifficulty);
        });

        $rootScope.$broadcast('menuEvent', '');

        if (gameData.getGameData() == null) {
            $scope.game = gameData.createGameData();
            $scope.startNewGame();
        } else {
            $scope.game = gameData.getGameData();
            boardStyle.updateBoardSize($scope.game.board.rows,
                                       $scope.game.board.columns);
            if ($scope.game.getState() == GAME_STARTED)
                gameTime.startTimer();
        }
    });

minesweeperApp.directive('ngRightClick',
    function ($parse) {
        return function (scope, element, attrs) {
            var fn = $parse(attrs.ngRightClick);
            element.bind('contextmenu', function (event) {
                scope.$apply(function () {
                    event.preventDefault();
                    fn(scope, { $event: event });
                });
            });
        };
    });