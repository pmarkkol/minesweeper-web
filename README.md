# Minesweeper-Web #

Javascript implementation of the classic Windows game Minesweeper where the goal is to find all empty tiles while avoiding mines.

### Requirements ###

Uses Javascript/AngularJS for the game/frontend and php for the backend to store best times.

Tested to work on latest (as of December 2016) versions of Firefox, Chrome and Opera.

### Contact ###

Paavo Markkola (paavo.markkola@iki.fi)